package yakkum.app;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import yakkum.app.Adapter.AdapterDataWarga;
import yakkum.app.Model.ModelDataWarga;
import yakkum.app.Util.AppController;
import yakkum.app.Util.JsonArrayPostRequest;
import yakkum.app.Util.ServerAPI;

public class ActivityWarga extends AppCompatActivity {
    RecyclerView mRecyclerview;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;
    List<ModelDataWarga> mItems;
    Spinner fildus;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_warga);

        pd = new ProgressDialog(ActivityWarga.this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityWarga.this, InsertDataWarga.class);
                startActivity(intent);
            }
        });

        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerviewTemp);

        fildus = (Spinner) findViewById(R.id.spin_dusun);
        final ArrayAdapter<String> adapterdus = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.filterdusun));
        fildus.setAdapter(adapterdus);
        fildus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                loadJson();
                //Toast.makeText(getApplicationContext(), fildus.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

                mItems = new ArrayList<>();


                mManager = new LinearLayoutManager(ActivityWarga.this,LinearLayoutManager.VERTICAL,false);
                mRecyclerview.setLayoutManager(mManager);
                mAdapter = new AdapterDataWarga(ActivityWarga.this,mItems);
                mRecyclerview.setAdapter(mAdapter);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });


    }


    private void loadJson()
    {
        pd.setMessage("Mengambil Data");
        pd.setCancelable(false);
        pd.show();
        Map<String, String> params = new HashMap<String, String>();
        params.put("fildus", fildus.getSelectedItem().toString());
        JsonArrayPostRequest reqData = new JsonArrayPostRequest(ServerAPI.URL_DATA_WARGA,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pd.cancel();
                        Log.d("volley","response : " + response.toString());
                        for(int i = 0 ; i < response.length(); i++)
                        {
                            try {
                                JSONObject data = response.getJSONObject(i);
                                ModelDataWarga md = new ModelDataWarga();
                                md.setId_Warga(data.getString("id_warga"));
                                md.setNo_Kk(data.getString("no_kk"));
                                md.setNama(data.getString("nama"));
                                md.setUmur(data.getString("umur"));
                                md.setJenisKelamin(data.getString("jeniskelamin"));
                                md.setDisabilitas(data.getString("disabilitas"));
                                md.setAlatBantu(data.getString("alatbantu"));
                                md.setDusun(data.getString("dusun"));
                                md.setAlamat(data.getString("alamat"));
                                md.setFoto(data.getString("foto"));
                                md.setLatitude(data.getString("latitude"));
                                md.setLongtitude(data.getString("longtitude"));
                                mItems.add(md);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Log.d("volley", "error : " + error.getMessage());
                    }
                },params);
        reqData.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(reqData);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityWarga.this);
        builder.setMessage("Apakah anda yakin ingin keluar ?");
        builder.setCancelable(true);
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

/*    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.opt_menu_warga, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tambah:
                Intent intent = new Intent(this, InsertDataWarga.class);
                startActivity(intent);
                return true;
            case R.id.keluar:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/


}
