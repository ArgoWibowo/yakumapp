package yakkum.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;

public class LoginActivity extends AppCompatActivity  {


    EditText Uname,Pass;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        Uname = (EditText) findViewById(R.id.uname);

        Pass = (EditText) findViewById(R.id.password);
        pd = new ProgressDialog(LoginActivity.this);

        Button SignInButton = (Button) findViewById(R.id.email_sign_in_button);
        SignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            Login();
            }
        });
    }

    private void Login()
    {

        pd.setMessage("Cek Login");
        pd.setCancelable(false);
        pd.show();

        StringRequest sendData = new StringRequest(Request.Method.POST, ServerAPI.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        Log.d("volley","response : " + response.toString());
                        try {
                            JSONObject res = new JSONObject(response);
                            String bdi = res.getString("code");
                            Toast.makeText(LoginActivity.this, "pesan : "+   res.getString("message") , Toast.LENGTH_SHORT).show();
                            if (bdi.equals("1")) {
                                startActivity( new Intent(LoginActivity.this,ActivityWarga.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //startActivity( new Intent(LoginActivity.this,MainActivity.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Toast.makeText(LoginActivity.this, "pesan : Gagal Login", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();

                map.put("uname",Uname.getText().toString());
                map.put("pass",Pass.getText().toString());


                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(sendData);
    }

}

