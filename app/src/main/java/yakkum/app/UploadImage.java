package yakkum.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;

public class UploadImage extends AppCompatActivity {
    Button btnUpload,btnPilih;
    ImageView imageView;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60; // range 1 - 100
    ProgressDialog pd;

    String intent_no_kk,intent_nama,intent_umur,intent_jeniskelamin,intent_disabilitas,intent_alatbantu,intent_dusun,intent_alamat,intent_latitude,intent_longtitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);

        /*get data from intent*/

        Intent data = getIntent();
        intent_no_kk = data.getStringExtra("no_kk");
        intent_nama = data.getStringExtra("nama");
        intent_umur = data.getStringExtra("umur");
        intent_jeniskelamin = data.getStringExtra("jeniskelamin");
        intent_disabilitas = data.getStringExtra("disabilitas");
        intent_alatbantu = data.getStringExtra("alatbantu");
        intent_dusun = data.getStringExtra("dusun");
        intent_alamat = data.getStringExtra("alamat");
        intent_latitude = data.getStringExtra("latitude");
        intent_longtitude = data.getStringExtra("longtitude");

        /*end get data from intent*/

        btnUpload = (Button) findViewById(R.id.button3);
        btnPilih = (Button) findViewById(R.id.button2);
        imageView = (ImageView) findViewById(R.id.imageView);

        pd = new ProgressDialog(UploadImage.this);

        btnPilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                //startActivityForResult(Intent.createChooser(intent,"Select Picture"),999);
                startActivityForResult(Intent.createChooser(intent,"Select Picture"),1);
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UploadImage();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if(requestCode == 999 && resultCode== RESULT_OK && data !=null){
        if(requestCode == 1 && resultCode== RESULT_OK && data !=null){
            Uri filepath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filepath);
                //imageView.setImageBitmap(bitmap);
                setToImageView(getResizedBitmap(bitmap, 512));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        imageView.setImageBitmap(decoded);
    }

    // fungsi resize image
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

public String getStringImage(Bitmap bm){
    ByteArrayOutputStream ba = new ByteArrayOutputStream();
    bm.compress(Bitmap.CompressFormat.JPEG,100,ba);
    byte[] imagebyte = ba.toByteArray();
    String encode = Base64.encodeToString(imagebyte,Base64.DEFAULT);
    return encode;
}

    private void UploadImage(){
        pd.setMessage("Menyimpan Data");
        pd.setCancelable(false);
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ServerAPI.URL_INSERT_WARGA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.cancel();
                try {
                    JSONObject res = new JSONObject(response);
                    Toast.makeText(UploadImage.this, "pesan : "+   res.getString("message") , Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(UploadImage.this, ActivityWarga.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.cancel();
                Toast.makeText(UploadImage.this, "pesan : Gagal Insert Data", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {;
                String image = getStringImage(bitmap);

                Map<String ,String> map = new HashMap<String,String>();

                map.put("no_kk",intent_no_kk);
                map.put("nama",intent_nama);
                map.put("umur",intent_umur);
                map.put("jeniskelamin",intent_jeniskelamin);
                map.put("disabilitas",intent_disabilitas);
                map.put("alatbantu",intent_alatbantu);
                map.put("dusun",intent_dusun);
                map.put("alamat",intent_alamat);
                map.put("foto",image);
                map.put("latitude",intent_latitude);
                map.put("longtitude",intent_longtitude);


                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //RequestQueue requestQueue = Volley.newRequestQueue(this);
        //requestQueue.add(stringRequest);
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

}
