package yakkum.app.Util;


    public class ServerAPI {
        //data login
        public static final String URL_LOGIN = "https://yakkum.000webhostapp.com/crud_android/login.php";

        //data warga
        public static final String URL_DATA_WARGA = "https://yakkum.000webhostapp.com/crud_android/view_data_warga_new.php";
        public static final String URL_INSERT_WARGA = "https://yakkum.000webhostapp.com/crud_android/create_data_warga.php";
        public static final String URL_DELETE_WARGA = "https://yakkum.000webhostapp.com/crud_android/delete_data_warga.php";
        public static final String URL_UPDATE_WARGA = "https://yakkum.000webhostapp.com/crud_android/update_data_warga.php";

        //data kebutuhan
        public static final String URL_DATA_KEB = "https://yakkum.000webhostapp.com/crud_android/view_data_keb_new.php";
        public static final String URL_INSERT_KEB = "https://yakkum.000webhostapp.com/crud_android/create_data_keb.php";
        public static final String URL_DELETE_KEB = "https://yakkum.000webhostapp.com/crud_android/delete_data_keb.php";
        public static final String URL_UPDATE_KEB = "https://yakkum.000webhostapp.com/crud_android/update_data_keb.php";
    }

