package yakkum.app;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;

public class InsertDataKeb extends AppCompatActivity {
    EditText id_keb, jenis, status, penanggung,waktu ;
    Button btnbatal,btnsimpan, btnwaktu;
    ProgressDialog pd;
    String intent_id_keb,intent_id_warga;

    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data_keb);

        /*get data from intent*/
        Intent data = getIntent();
        final int update = data.getIntExtra("update",0);
         intent_id_keb        = data.getStringExtra("id_keb");
         intent_id_warga      =data.getStringExtra("id_warga");
        String intent_jenis         = data.getStringExtra("jenis");
        String intent_status        = data.getStringExtra("status");
        String intent_penanggung    = data.getStringExtra("penanggung");
        String intent_waktu         = data.getStringExtra("waktu");

        /*end get data from intent*/

        jenis = (EditText) findViewById(R.id.inp_jenis);
        status = (EditText) findViewById(R.id.inp_status);
        penanggung = (EditText) findViewById(R.id.inp_penanggung);
        waktu = (EditText) findViewById(R.id.inp_waktu);


        btnbatal = (Button) findViewById(R.id.btn_cancel);
        btnsimpan = (Button) findViewById(R.id.btn_simpan);
        btnwaktu = (Button) findViewById(R.id.btn_waktu);
        pd = new ProgressDialog(InsertDataKeb.this);

        /*kondisi update / insert*/
        if(update == 1)
        {
            String[] parts = intent_penanggung.split("[.]");
            String part1 = parts[1];
            btnsimpan.setText("Update Data");
            jenis.setText(intent_jenis);
            status.setText(intent_status);
            penanggung.setText(part1);
            waktu.setText(intent_waktu);

        }

        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        btnwaktu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(InsertDataKeb.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(update == 1)
                {
                    if(jenis.length()==0 || status.length()==0 || penanggung.length()==0 || waktu.length()==0){
                        Toast.makeText(InsertDataKeb.this, "Tidak boleh kosong" , Toast.LENGTH_SHORT).show();
                    }else {
                        Update_data();
                    }
                }else {
                    if(jenis.length()==0 || status.length()==0 || penanggung.length()==0 || waktu.length()==0){
                        Toast.makeText(InsertDataKeb.this, "Tidak boleh kosong" , Toast.LENGTH_SHORT).show();
                    }else {
                        simpanData();
                    }
                }
            }
        });

        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(InsertDataKeb.this,ActivityKeb.class);
                main.putExtra("id_warga",intent_id_warga);
                main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(main);
            }
        });
    }

    private void updateLabel(){
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        waktu.setText(sdf.format(myCalendar.getTime()));
    }

    private void Update_data()
    {
        pd.setMessage("Update Data");
        pd.setCancelable(false);
        pd.show();

        StringRequest updateReq = new StringRequest(Request.Method.POST, ServerAPI.URL_UPDATE_KEB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        try {
                            JSONObject res = new JSONObject(response);
                            Toast.makeText(InsertDataKeb.this, "pesan : "+   res.getString("message") , Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(InsertDataKeb.this, ActivityKeb.class);
                        intent.putExtra("id_warga",intent_id_warga);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        //startActivity( new Intent(InsertDataKeb.this,ActivityKeb.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Toast.makeText(InsertDataKeb.this, "pesan : Gagal Update Data", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
Map<String,String> map = new HashMap<>();
                map.put("id_keb",intent_id_keb);
                map.put("id_warga",intent_id_warga);
                map.put("jenis",jenis.getText().toString());
                map.put("status",status.getText().toString());
                map.put("penanggung",penanggung.getText().toString());
                map.put("waktu",waktu.getText().toString());

                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(updateReq);
    }



    private void simpanData()
    {

        pd.setMessage("Menyimpan Data");
        pd.setCancelable(false);
        pd.show();

        StringRequest sendData = new StringRequest(Request.Method.POST, ServerAPI.URL_INSERT_KEB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        try {
                            JSONObject res = new JSONObject(response);
                            Toast.makeText(InsertDataKeb.this, "pesan : "+   res.getString("message") , Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(InsertDataKeb.this, ActivityKeb.class);
                        intent.putExtra("id_warga",intent_id_warga);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        //startActivity( new Intent(InsertDataKeb.this,ActivityKeb.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Toast.makeText(InsertDataKeb.this, "pesan : Gagal Insert Data", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();

                map.put("id_warga",intent_id_warga);
                map.put("jenis",jenis.getText().toString());
                map.put("status",status.getText().toString());
                map.put("penanggung",penanggung.getText().toString());
                map.put("waktu",waktu.getText().toString());


                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(sendData);
    }

    @Override
    public void onBackPressed() {

    }
}
