package yakkum.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;


public class InsertDataWarga extends AppCompatActivity {
    EditText id_warga, no_kk, nama, umur, jeniskelamin, disabilitas, alatbantu, dusun, alamat, foto, latitude, longtitude ;
    Spinner dus;
    RadioButton jk_p,jk_w;
    Button btnbatal,btnsimpan;
    ProgressDialog pd;
    String intent_id_warga;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_data_warga);

        /*get data from intent*/
        Intent data = getIntent();
        final int update = data.getIntExtra("update",0);
        intent_id_warga = data.getStringExtra("id_warga");
        String intent_no_kk = data.getStringExtra("no_kk");
        String intent_nama = data.getStringExtra("nama");
        String intent_umur = data.getStringExtra("umur");
        String intent_jeniskelamin = data.getStringExtra("jeniskelamin");
        String intent_disabilitas = data.getStringExtra("disabilitas");
        String intent_alatbantu = data.getStringExtra("alatbantu");
        String intent_dusun = data.getStringExtra("dusun");
        String intent_alamat = data.getStringExtra("alamat");
        //String intent_latitude = data.getStringExtra("latitude");
        //String intent_longtitude = data.getStringExtra("longtitude");
        /*end get data from intent*/

        no_kk = (EditText) findViewById(R.id.inp_no_kk);
        nama = (EditText) findViewById(R.id.inp_nama);
        umur = (EditText) findViewById(R.id.inp_umur);
        /*jeniskelamin = (EditText) findViewById(R.id.inp_jeniskelamin);*/
        jk_p = (RadioButton) findViewById(R.id.jk_p);
        jk_w = (RadioButton) findViewById(R.id.jk_w);
        disabilitas = (EditText) findViewById(R.id.inp_disabilitas);
        alatbantu = (EditText) findViewById(R.id.inp_alatbantu);
        /*dusun = (EditText) findViewById(R.id.inp_dusun);*/
        dus = (Spinner) findViewById(R.id.spin_dusun);
        alamat = (EditText) findViewById(R.id.inp_alamat);
        //latitude = (EditText) findViewById(R.id.inp_latitude);
        //longtitude = (EditText) findViewById(R.id.inp_longtitude);

        btnbatal = (Button) findViewById(R.id.btn_cancel);
        btnsimpan = (Button) findViewById(R.id.btn_simpan);

        final ArrayAdapter<String> adapterdus = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.dusun));
        dus.setAdapter(adapterdus);

        pd = new ProgressDialog(InsertDataWarga.this);

        /*kondisi update / insert*/
        if(update == 1)
        {
            btnsimpan.setText("Update Data");
            no_kk.setText(intent_no_kk);
            nama.setText(intent_nama);
            umur.setText(intent_umur);

            /*jeniskelamin.setText(intent_jeniskelamin);*/
            if(intent_jeniskelamin.equals("L")){
                jk_p.setChecked(true);
            }else{
                jk_w.setChecked(true);
            }

            disabilitas.setText(intent_disabilitas);
            alatbantu.setText(intent_alatbantu);

            /*dusun.setText(intent_dusun);*/
            int spinnerPosition = 0;
            spinnerPosition = adapterdus.getPosition(intent_dusun);
            dus.setSelection(spinnerPosition);

            alamat.setText(intent_alamat);
            //latitude.setText(intent_latitude);
            //longtitude.setText(intent_longtitude);
        }else{
            btnsimpan.setText("Lanjut");
        }


        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(update == 1)
                {
                    if(no_kk.length()==0 || nama.length()==0 || umur.length()==0 || disabilitas.length()==0 || alatbantu.length()==0 || alamat.length()==0){
                        Toast.makeText(InsertDataWarga.this, "Tidak boleh kosong" , Toast.LENGTH_SHORT).show();
                    }else {
                        Update_data();
                    }
                }else {
                    if(no_kk.length()==0 || nama.length()==0 || umur.length()==0 || disabilitas.length()==0 || alatbantu.length()==0 || alamat.length()==0){
                        Toast.makeText(InsertDataWarga.this, "Tidak boleh kosong" , Toast.LENGTH_SHORT).show();
                    }else {
                        next_data();
                    }
                }
            }
        });

        btnbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(InsertDataWarga.this,ActivityWarga.class);
                main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(main);
            }
        });
    }

    private void next_data()
    {
        Intent main = new Intent(InsertDataWarga.this,InsertLokasi.class);
        main.putExtra("no_kk",no_kk.getText().toString());
        main.putExtra("nama",nama.getText().toString());
        main.putExtra("umur",umur.getText().toString());

        /*main.putExtra("jeniskelamin",jeniskelamin.getText().toString());*/
        if(jk_p.isChecked()){
            main.putExtra("jeniskelamin","L");
        }else{
            main.putExtra("jeniskelamin","P");
        }

        main.putExtra("disabilitas", disabilitas.getText().toString());
        main.putExtra("alatbantu", alatbantu.getText().toString());

        /*main.putExtra("dusun", dusun.getText().toString());*/
        main.putExtra("dusun", dus.getSelectedItem().toString());

        main.putExtra("alamat", alamat.getText().toString());
        //main.putExtra("latitude", latitude.getText().toString());
        //main.putExtra("longtitude", longtitude.getText().toString());
        startActivity(main);
    }

    private void Update_data()
    {
        pd.setMessage("Update Data");
        pd.setCancelable(false);
        pd.show();

        StringRequest updateReq = new StringRequest(Request.Method.POST, ServerAPI.URL_UPDATE_WARGA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        try {
                            JSONObject res = new JSONObject(response);
                            Toast.makeText(InsertDataWarga.this, "pesan : "+   res.getString("message") , Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(InsertDataWarga.this, ActivityWarga.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        //startActivity( new Intent(InsertDataWarga.this,ActivityWarga.class));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Toast.makeText(InsertDataWarga.this, "pesan : Gagal Update Data", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
Map<String,String> map = new HashMap<>();
                map.put("id_warga",intent_id_warga);
                map.put("no_kk",no_kk.getText().toString());
                map.put("nama",nama.getText().toString());
                map.put("umur",umur.getText().toString());

                /*map.put("jeniskelamin",jeniskelamin.getText().toString());*/
                if(jk_p.isChecked()){
                    map.put("jeniskelamin","L");
                }else{
                    map.put("jeniskelamin","P");
                }

                map.put("disabilitas",disabilitas.getText().toString());
                map.put("alatbantu",alatbantu.getText().toString());

                /*map.putExtra("dusun", dusun.getText().toString());*/
                map.put("dusun", dus.getSelectedItem().toString());

                map.put("alamat",alamat.getText().toString());
                //map.put("latitude",latitude.getText().toString());
                //map.put("longtitude",longtitude.getText().toString());

                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(updateReq);
    }

    @Override
    public void onBackPressed() {

    }
}
