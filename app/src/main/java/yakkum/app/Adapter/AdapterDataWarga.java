package yakkum.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import yakkum.app.ActivityKeb;
import yakkum.app.ActivityWarga;
import yakkum.app.InsertDataKeb;
import yakkum.app.InsertDataWarga;
import yakkum.app.Model.ModelDataWarga;
import yakkum.app.R;
import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AdapterDataWarga extends RecyclerView.Adapter<AdapterDataWarga.HolderData> {
    private List<ModelDataWarga> mItems ;
    private Context context;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    ProgressDialog pd;

    public AdapterDataWarga(Context context, List<ModelDataWarga> items)
    {
        this.mItems = items;
        this.context = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        //View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_row_warga,parent,false);
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_warga,parent,false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        ModelDataWarga md  = mItems.get(position);
/*        holder.tvno_kk.setText(md.getNo_Kk());
        holder.tvid_warga.setText(md.getId_Warga());*/

        holder.thumbNail.setImageUrl(md.getFoto(), imageLoader);
        holder.tvno_kk.setText(md.getNo_Kk());
        holder.tvnama.setText(md.getNama());
        holder.tvdusun.setText(md.getDusun());
        holder.tvjk.setText(md.getJenisKelamin());


        holder.md = md;


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class HolderData extends RecyclerView.ViewHolder
    {
        NetworkImageView thumbNail;
        TextView tvno_kk,tvnama,tvdusun,tvjk;
        ModelDataWarga md;

        public  HolderData (View view)
        {
            super(view);
            thumbNail = (NetworkImageView) view.findViewById (R.id.thumbnail);
            tvno_kk = (TextView) view.findViewById(R.id.no_kk);
            tvnama = (TextView) view.findViewById(R.id.nama);
            tvdusun = (TextView) view.findViewById(R.id.dusun);
            tvjk = (TextView) view.findViewById(R.id.jk);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] items = {"Edit", "Hapus", "Input Kebutuhan"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);

                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {

                            switch (item){
                                case 0:
                                    Intent update = new Intent(context, InsertDataWarga.class);
                                    update.putExtra("update",1);
                                    update.putExtra("id_warga",md.getId_Warga());
                                    update.putExtra("no_kk",md.getNo_Kk());
                                    update.putExtra("nama",md.getNama());
                                    update.putExtra("umur",md.getUmur());
                                    update.putExtra("jeniskelamin",md.getJenisKelamin());
                                    update.putExtra("disabilitas", md.getDisabilitas());
                                    update.putExtra("alatbantu", md.getAlatBantu());
                                    update.putExtra("dusun", md.getDusun());
                                    update.putExtra("alamat", md.getAlamat());
                                    //update.putExtra("foto", md.getFoto());
                                    update.putExtra("latitude", md.getLatitude());
                                    update.putExtra("longtitude", md.getLongtitude());
                                    context.startActivity(update);
                                break;

                                case 1:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Yakin akan dihapus ?");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            deleteData(md.getId_Warga());
                                        }
                                    });

                                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();

                                break;

                                case 2:
                                    Intent keb = new Intent(context, ActivityKeb.class);
                                    keb.putExtra("id_warga",md.getId_Warga());
                                    context.startActivity(keb);
                                break;

                            }

                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    private void deleteData(final String id_warga)
    {
        pd = new ProgressDialog(context);
        pd.setMessage("Delete Warga Data ...");
        pd.setCancelable(false);
        pd.show();

        StringRequest delReq = new StringRequest(Request.Method.POST, ServerAPI.URL_DELETE_WARGA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        Log.d("volley","response : " + response.toString());
                        try {
                            JSONObject res = new JSONObject(response);
                            Toast.makeText(context,"pesan : " +res.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(context, ActivityWarga.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Log.d("volley", "error : " + error.getMessage());
                        Toast.makeText(context, "pesan : gagal menghapus data", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id_warga",id_warga.toString());
                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(delReq);
    }
}
