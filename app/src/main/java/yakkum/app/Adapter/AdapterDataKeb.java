package yakkum.app.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import yakkum.app.ActivityKeb;
import yakkum.app.InsertDataKeb;
import yakkum.app.Model.ModelDataKeb;
import yakkum.app.R;
import yakkum.app.Util.AppController;
import yakkum.app.Util.ServerAPI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AdapterDataKeb extends RecyclerView.Adapter<AdapterDataKeb.HolderData> {
    private List<ModelDataKeb> mItems ;
    private Context context;
    ProgressDialog pd;

    public AdapterDataKeb(Context context, List<ModelDataKeb> items)
    {
        this.mItems = items;
        this.context = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_keb,parent,false);
        HolderData holderData = new HolderData(layout);
        return holderData;
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        ModelDataKeb md  = mItems.get(position);
        holder.tvjenis.setText(md.getJenis());
        holder.tvstatus.setText(md.getStatus());
        holder.tvpj.setText(md.getPenanggung());
        holder.tvwaktu.setText(md.getWaktu());

        holder.md = md;


    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class HolderData extends RecyclerView.ViewHolder
    {
        TextView tvjenis,tvstatus,tvpj,tvwaktu;
        ModelDataKeb md;

        public  HolderData (View view)
        {
            super(view);

            tvjenis = (TextView) view.findViewById(R.id.jenis);
            tvstatus = (TextView) view.findViewById(R.id.status);
            tvpj = (TextView) view.findViewById(R.id.pj);
            tvwaktu = (TextView) view.findViewById(R.id.waktu);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] items = {"Edit", "Hapus"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {

                            switch (item){
                                case 0:
                                    Intent update = new Intent(context, InsertDataKeb.class);
                                    update.putExtra("update",1);
                                    update.putExtra("id_keb",md.getId_Keb());
                                    update.putExtra("id_warga",md.getId_warga());
                                    update.putExtra("jenis",md.getJenis());
                                    update.putExtra("status",md.getStatus());
                                    update.putExtra("penanggung",md.getPenanggung());
                                    update.putExtra("waktu",md.getWaktu());

                                    context.startActivity(update);
                                    break;

                                case 1:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setMessage("Yakin akan dihapus ?");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            deleteData(md.getId_Keb(), md.getId_warga());
                                        }
                                    });

                                    builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                    break;

                            }

                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }
    }

    private void deleteData(final String id_keb, final String id_warga)
    {
        pd = new ProgressDialog(context);
        pd.setMessage("DeleteKeb Data ...");
        pd.setCancelable(false);
        pd.show();

        StringRequest delReq = new StringRequest(Request.Method.POST, ServerAPI.URL_DELETE_KEB,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pd.cancel();
                        Log.d("volley","response : " + response.toString());
                        try {
                            JSONObject res = new JSONObject(response);
                            Toast.makeText(context,"pesan : " +res.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(context, ActivityKeb.class);
                        intent.putExtra("id_warga",id_warga);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Log.d("volley", "error : " + error.getMessage());
                        Toast.makeText(context, "pesan : gagal menghapus data", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("id_keb",id_keb);
                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(delReq);
    }
}
