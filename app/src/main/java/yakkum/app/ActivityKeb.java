package yakkum.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import yakkum.app.Adapter.AdapterDataKeb;
import yakkum.app.Model.ModelDataKeb;
import yakkum.app.Util.AppController;
import yakkum.app.Util.JsonArrayPostRequest;
import yakkum.app.Util.ServerAPI;

public class ActivityKeb extends AppCompatActivity {
    RecyclerView mRecyclerview;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;
    List<ModelDataKeb> mItems;
    ProgressDialog pd;
    String intent_id_warga;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_keb);

        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerviewTemp);
        pd = new ProgressDialog(ActivityKeb.this);
        mItems = new ArrayList<>();

        Intent data = getIntent();
        intent_id_warga = data.getStringExtra("id_warga");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityKeb.this, InsertDataKeb.class);
                intent.putExtra("id_warga",intent_id_warga);
                startActivity(intent);
            }
        });

        loadJson();

        mManager = new LinearLayoutManager(ActivityKeb.this, LinearLayoutManager.VERTICAL,false);
        mRecyclerview.setLayoutManager(mManager);
        mAdapter = new AdapterDataKeb(ActivityKeb.this,mItems);
        mRecyclerview.setAdapter(mAdapter);

    }


    private void loadJson()
    {
        pd.setMessage("Mengambil Data");
        pd.setCancelable(false);
        pd.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("id_warga", intent_id_warga);
        JsonArrayPostRequest reqData = new JsonArrayPostRequest(ServerAPI.URL_DATA_KEB,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pd.cancel();
                        Log.d("volley","response : " + response.toString());
                        for(int i = 0 ; i < response.length(); i++)
                        {
                            try {
                                JSONObject data = response.getJSONObject(i);
                                ModelDataKeb md = new ModelDataKeb();
                                md.setId_Keb(data.getString("id_keb"));
                                md.setId_warga(data.getString("id_warga"));
                                md.setJenis(data.getString("jenis"));
                                md.setStatus(data.getString("status"));
                                md.setPenanggung("PenanggungJawab. "+data.getString("penanggung"));
                                md.setWaktu(data.getString("waktu"));

                                mItems.add(md);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.cancel();
                        Log.d("volley", "error : " + error.getMessage());
                    }
                },params);

        AppController.getInstance().addToRequestQueue(reqData);
    }

/*    @Override
    public void onBackPressed() {

    }*/

/*    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.opt_menu_keb, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tambah:
                Intent intent = new Intent(this, InsertDataKeb.class);
                intent.putExtra("id_warga",intent_id_warga);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

}
