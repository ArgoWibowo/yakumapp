package yakkum.app.Model;

/**
 * Created by hakiki95 on 11/30/2016.
 */

public class ModelDataKeb {
    String id_keb, id_warga, jenis, status, penanggung, waktu;

    public ModelDataKeb(){}

    public ModelDataKeb(String id_keb, String id_warga, String jenis, String status, String penanggung, String waktu) {
        this.id_keb = id_keb;
        this.id_warga = id_warga;
        this.jenis          = jenis;
        this.status         = status;
        this.penanggung     = penanggung;
        this.waktu          = waktu;

    }

    public String getId_Keb() {
        return id_keb;
    }

    public void setId_Keb(String id_keb) {
        this.id_keb = id_keb;
    }

    public String getId_warga() {
        return id_warga;
    }

    public void setId_warga(String id_warga) {
        this.id_warga = id_warga;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPenanggung() {
        return penanggung;
    }

    public void setPenanggung(String penanggung) {
        this.penanggung = penanggung;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

}
