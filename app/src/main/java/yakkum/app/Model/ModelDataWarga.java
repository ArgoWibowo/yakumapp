package yakkum.app.Model;

/**
 * Created by hakiki95 on 11/30/2016.
 */

public class ModelDataWarga {
    String id_warga, no_kk, nama, umur, jeniskelamin, disabilitas, alatbantu, dusun, alamat, foto, latitude, longtitude;

    public ModelDataWarga(){}

    public ModelDataWarga(String id_warga, String no_kk, String nama, String umur, String jeniskelamin, String disabilitas, String alatbantu, String dusun, String alamat, String foto, String latitude, String longtitude) {
        this.id_warga = id_warga;
        this.no_kk          = no_kk;
        this.nama           = nama;
        this.umur           = umur;
        this.jeniskelamin   = jeniskelamin;
        this.disabilitas    = disabilitas;
        this.alatbantu      = alatbantu;
        this.dusun          = dusun;
        this.alamat         = alamat;
        this.foto           = foto;
        this.latitude       = latitude;
        this.longtitude     = longtitude;
    }

    public String getId_Warga() {
        return id_warga;
    }

    public void setId_Warga(String id_warga) {
        this.id_warga = id_warga;
    }

    public String getNo_Kk() {
        return no_kk;
    }

    public void setNo_Kk(String no_kk) {
        this.no_kk = no_kk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public String getJenisKelamin() {
        return jeniskelamin;
    }

    public void setJenisKelamin(String jeniskelamin) {
        this.jeniskelamin = jeniskelamin;
    }

    public String getDisabilitas() {
        return disabilitas;
    }

    public void setDisabilitas(String disabilitas) {
        this.disabilitas = disabilitas;
    }

    public String getAlatBantu() {
        return alatbantu;
    }

    public void setAlatBantu(String alatbantu) {
        this.alatbantu = alatbantu;
    }

    public String getDusun() {
        return dusun;
    }

    public void setDusun(String dusun) {
        this.dusun = dusun;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }
}
